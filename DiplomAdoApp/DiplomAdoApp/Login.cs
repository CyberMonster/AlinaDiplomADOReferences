﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DiplomAdoApp.AppData;

namespace DiplomAdoApp
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConnectionInfo.UserName = this.textBox1.Text;
            ConnectionInfo.UserPassword = this.textBox2.Text;
            this.Hide();
            FormsList.MainForm = new MainPresentation();
            FormsList.MainForm.Show();
            FormsList.MainForm.Focus();
        }
    }
}
