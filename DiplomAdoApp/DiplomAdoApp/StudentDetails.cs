﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DiplomAdoApp.MySqlFunctions;
using DiplomAdoApp.DataTypes;

namespace DiplomAdoApp
{
    public partial class StudentDetails : Form
    {
        public StudentDetails()
        {
            InitializeComponent();
        }

        private void SetNotChangenedDataToControls()
        {
            var Rows_ = DBConnector.GetDate(@"SELECT * FROM `host1565314_mt`.`pol`").DefaultView.Table.Rows;

            this.PolData = new List<PolDataClass>();

            for (var i = 0; i < Rows_.Count; ++i)
            {
                PolData.Add(new PolDataClass(int.Parse(Rows_[i].ItemArray[0].ToString()), Rows_[i].ItemArray[1].ToString()));
            }

            this.comboBox1.DataSource = this.PolData;
            this.comboBox1.DisplayMember = @"Male";
        }

        private void StudentDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void StudentDetails_Activated(object sender, EventArgs e)
        {
            ///activated, when form focused
            SetNotChangenedDataToControls();
        }

        public int StudentId;

        public List<PolDataClass> PolData;
    }
}
