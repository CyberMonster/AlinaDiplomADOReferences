﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DiplomAdoApp.MySqlFunctions;
using DiplomAdoApp.AppData;

namespace DiplomAdoApp
{
    public partial class MainWorkSpace : Form
    {
        public MainWorkSpace(string Otdelenie, string IdOtdelenie)
        {
            InitializeComponent();

            this.IdOtdelenie = IdOtdelenie;

            this.CommonQuery = @"SELECT Fam AS 'Фамилия', Ima AS 'Имя',Otch AS 'Отчество', Sokr_nazv AS 'Пол', Data_rogd AS 'Дата рождения', Lich_delo AS '№ личного дела',
                                Kod_gruppa AS 'Идентификатор группы', Student.Telephone AS '№ телефона',
                                sostoyaniye.Nazvanie AS 'Состояние обучения', ID_kurs AS 'Курс обучения'
                                FROM `Student` JOIN `pol` ON pol.ID_pol = Student.ID_pol JOIN `gruppa` ON gruppa.ID_gruppa = Student.ID_gruppa JOIN `sostoyaniye` ON sostoyaniye.ID_sost = Student.ID_sost
                                JOIN `forma_obuch` ON forma_obuch.ID_forma = Student.ID_forma JOIN `obrazovanie` ON obrazovanie.ID_obraz = Student.ID_obraz
                                JOIN `otdelenie` ON otdelenie.ID_otdel = Student.ID_otdel JOIN `roditely` ON roditely.ID_roditel = Student.ID_roditel
                                JOIN `dokumenty` ON dokumenty.ID_dok = Student.ID_dok WHERE Student.ID_otdel = " + this.IdOtdelenie;
            this.CommonQueryEndler = @" ORDER BY ID_student LIMIT 0, 10000;";

            this.ParentSearchItemsChecked = new List<int>();
            this.ParentSearchBufferBox = new CheckedListBox();
            this.checkedListBox1.SetItemChecked(0, true);
            this.ParentSearchBufferBox.Items.Add(this.checkedListBox1.Items[0]);
            this.ParentSearchBufferBox.Items.Add(this.checkedListBox1.Items[8]);
            this.ParentSearchItemsChecked.Add(0);
            this.Text = Otdelenie;
            this.dataGridView2.DataSource = DBConnector.GetDate(this.CommonQuery + this.CommonQueryEndler);
            this.textBox4.Text = this.dataGridView2.RowCount.ToString();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                string Query = "";
                var SearchItems = this.textBox2.Text.Replace("/;", "{/_}").Split(';').Select(x => x.Replace("{/_}", ";")).ToArray();
                int SearchItemId = 0;
                if (this.checkBox1.Checked)
                {
                    foreach (var CheckedItem in this.checkedListBox1.CheckedItems)
                    {
                        switch (this.checkedListBox1.Items.IndexOf(CheckedItem))
                        {
                            case 0:
                                var FIO = SearchItems[SearchItemId].Replace("/ ", "{/_}").Split(' ').Select(x => x.Replace("{/_}", " ")).ToArray();
                                switch (FIO.Length)
                                {
                                    case 1:
                                        Query += @" AND roditely.FIO like '%" + FIO[0] + "%'";
                                        break;
                                    case 2:
                                        Query += @" AND roditely.FIO like '%" + FIO[0] + @"%' AND roditely.FIO like '%" + FIO[1] + "%'";
                                        break;
                                    case 3:
                                        Query += @" AND roditely.FIO like '%" + FIO[0] + @"%' roditely.FIO like '%" + FIO[1] + @"%' AND roditely.FIO like '%" + FIO[2] + "%'";
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 1:
                                Query += @" AND roditely.Telephone like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            default:
                                break;
                        }
                        ++SearchItemId;
                        if (SearchItemId == SearchItems.Length)
                        {
                            --SearchItemId;
                        }
                    }
                }
                else
                {
                    foreach (var CheckedItem in this.checkedListBox1.CheckedItems)
                    {
                        switch (this.checkedListBox1.Items.IndexOf(CheckedItem))
                        {
                            case 0:
                                var FIO = SearchItems[SearchItemId].Replace("/ ", "_").Split(' ').Select(x => x.Replace("_", " ")).ToArray();
                                switch (FIO.Length)
                                {
                                    case 1:
                                        Query += @" AND `Fam` like '%" + FIO[0] + "%'";
                                        break;
                                    case 2:
                                        Query += @" AND `Fam` like '%" + FIO[0] + @"%' AND Student.Ima like '%" + FIO[1] + "%'";
                                        break;
                                    case 3:
                                        Query += @" AND `Fam` like '%" + FIO[0] + @"%' AND Student.Ima like '%" + FIO[1] + @"%' AND Student.Otch like '%" + FIO[2] + "%'";
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 1:
                                Query += @" AND Student.Ima like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 2:
                                Query += @" AND Student.Otch like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 3:
                                Query += @" AND gruppa.Kod_gruppa like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 4:
                                Query += @" AND Student.ID_kurs like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 5:
                                Query += @" AND ID_godpost like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 6:
                                var PassData = SearchItems[SearchItemId].Split(' ');
                                switch (PassData.Length)
                                {
                                    case 1:
                                        Query += @" AND dokumenty.Seriya_pasp like '%" + PassData[0] + "%'";
                                        break;
                                    case 2:
                                        Query += @" AND dokumenty.Seriya_pasp like '%" + PassData[0] + @"%' AND dokumenty.Number_pasp like '%" + PassData[1] + "%'";
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case 7:
                                Query += @" AND dokumenty.Number_pasp like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            case 8:
                                Query += @" AND Student.Telephone like '%" + SearchItems[SearchItemId] + "%'";
                                break;
                            default:
                                break;
                        }
                        ++SearchItemId;
                        if (SearchItemId == SearchItems.Length)
                        {
                            --SearchItemId;
                        }
                    }
                }
                if (Query != "")
                {
                    this.dataGridView2.DataSource = DBConnector.GetDate(this.CommonQuery + Query + this.CommonQueryEndler);
                    this.textBox4.Text = this.dataGridView2.RowCount.ToString();
                }
            }
        }

        private void dataGridView2_DoubleClick(object sender, EventArgs e)
        {
            if (this.dataGridView2.SelectedRows.Count > 0 && this.dataGridView2.SelectedRows[0] == this.dataGridView2.CurrentRow)
            {
                int.TryParse((string)(this.dataGridView2.CurrentRow.Cells[0].FormattedValue), out FormsList.StudentDetailsForm.StudentId);
                FormsList.StudentDetailsForm.Show();
                FormsList.StudentDetailsForm.Focus();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            object[] ItemsBuffer = new object[this.checkedListBox1.Items.Count];
            List<int> CheckedItemsIndex = new List<int>();

            foreach (var CheckedItem in this.checkedListBox1.CheckedItems)
            {
                CheckedItemsIndex.Add(this.checkedListBox1.Items.IndexOf(CheckedItem));
            }

            this.checkedListBox1.Items.CopyTo(ItemsBuffer, 0);
            this.checkedListBox1.Items.Clear();
            this.checkedListBox1.Items.AddRange(this.ParentSearchBufferBox.Items);

            foreach (var CheckedIndex in this.ParentSearchItemsChecked)
            {
                this.checkedListBox1.SetItemChecked(CheckedIndex, true);
            }

            this.ParentSearchItemsChecked = CheckedItemsIndex;

            this.ParentSearchBufferBox.Items.Clear();
            this.ParentSearchBufferBox.Items.AddRange(ItemsBuffer);
        }


        private string IdOtdelenie;
        public string CommonQuery;
        public string CommonQueryEndler;

        private CheckedListBox ParentSearchBufferBox;
        private List<int> ParentSearchItemsChecked;
    }
}