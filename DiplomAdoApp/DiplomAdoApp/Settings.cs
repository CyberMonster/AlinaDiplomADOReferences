﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DiplomAdoApp.AppData;

namespace DiplomAdoApp
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
            this.textBox1.Text = ConnectionInfo.ServerIP;
            this.textBox2.Text = ConnectionInfo.DataBaseName;
        }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            ConnectionInfo.ServerIP = this.textBox1.Text;
            ConnectionInfo.DataBaseName = this.textBox2.Text;
            FormsList.MainForm.SettingsRefresh();
        }
    }
}
