﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomAdoApp.DataTypes
{
    public class PolDataClass
    {
        public PolDataClass(int MaleUID, string Male)
        {
            this.MaleUID = MaleUID;
            this.Male = Male;
        }

        private int MaleUID_;
        private string Male_;

        public int MaleUID
        {
            get
            {
                return this.MaleUID_;
            }
            set
            {
                this.MaleUID_ = value;
            }
        }
        public string Male
        {
            get
            {
                return this.Male_;
            }
            set
            {
                this.Male_ = value;
            }
        }
    }
}
