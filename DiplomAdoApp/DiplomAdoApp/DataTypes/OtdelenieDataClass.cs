﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomAdoApp.DataTypes
{
    class OtdelenieDataClass
    {
        public OtdelenieDataClass(string UIDOtdel, string Title, string FIOResponseblePerson, string PhoneNumber)
        {
            this.UIDOtdel = UIDOtdel;
            this.Nazvanie = Title;
            this.FIO_zav = FIOResponseblePerson;
            this.Telephone = PhoneNumber;
        }

        private string UIDOtdel_;
        private string Nazvanie_;
        private string FIO_zav_;
        private string Telephone_;

        public string UIDOtdel
        {
            get
            {
                return this.UIDOtdel_;
            }
            set
            {
                this.UIDOtdel_ = value;
            }
        }

        public string Nazvanie
        {
            get
            {
                return this.Nazvanie_;
            }
            set
            {
                this.Nazvanie_ = value;
            }
        }

        public string FIO_zav
        {
            get
            {
                return this.FIO_zav_;
            }
            set
            {
                this.FIO_zav_ = value;
            }
        }

        public string Telephone
        {
            get
            {
                return this.Telephone_;
            }
            set
            {
                this.Telephone_ = value;
            }
        }
    }
}
