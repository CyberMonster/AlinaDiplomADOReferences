﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

using DiplomAdoApp.DataTypes;
using DiplomAdoApp.AppData;
using DiplomAdoApp.MySqlFunctions;

namespace DiplomAdoApp
{
    public partial class MainPresentation : Form
    {
        public MainPresentation()
        {
            InitializeComponent();
            ListViewInitialize();
        }

        private void ListViewInitialize()
        {
            var RowsResult = DBConnector.GetDate(@"SELECT * FROM `Otdelenie`").Rows;

            //SetNotChangenedDataToControls();

            for (var i = 0; i < RowsResult.Count; ++i)
            {
                this.listView1.Items.Add(new ListViewItem(RowsResult[i].ItemArray[1].ToString(), RowsResult[i].ItemArray[0].ToString()) { BackColor = Color.Tan });
            }
        }

        /*private void SetNotChangenedDataToControls()
        {
            var Rows_ = DBConnector.GetDate(@"SELECT * FROM `host1565314_mt`.`otdelenie`").DefaultView.Table.Rows;

            this.OtdelenieData = new List<OtdelenieDataClass>();

            for (var i = 0; i < Rows_.Count; ++i)
            {
                OtdelenieData.Add(new OtdelenieDataClass(int.Parse(Rows_[i].ItemArray[0].ToString()), Rows_[i].ItemArray[1].ToString()));
            }

            this.listView1.DataSource = this.PolData;
            this.listView1.DisplayMember = @"Male";
        }*/

        /// <summary>
        /// Handler of settings button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (FormsList.SettingsForm.Handle == null)
                {
                    ;
                }
            }
            catch
            {
                FormsList.SettingsForm = new DiplomAdoApp.Settings();
                System.GC.Collect();
            }
            FormsList.SettingsForm.Show();
            FormsList.SettingsForm.Focus();
        }

        /// <summary>
        /// Refresh or set connection to DataBase
        /// </summary>
        public void SettingsRefresh()
        {
            ;
        }

        private void MainPresentation_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormsList.LoginForm.Close();
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                new MainWorkSpace(this.listView1.SelectedItems[0].Text, this.listView1.SelectedItems[0].ImageKey).Show();
            }
        }

        private List<OtdelenieDataClass> OtdelenieData;
    }
}