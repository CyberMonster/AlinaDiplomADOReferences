﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;

using DiplomAdoApp.MySqlFunctions;

namespace DiplomAdoApp.AppData
{
    class ConnectionInfo
    {
        /// <summary>
        /// Server IPv4
        /// </summary>
        public static string ServerIP = ServerInfo.DBUrl;
        /// <summary>
        /// Name of needed database
        /// </summary>
        public static string DataBaseName = ServerInfo.DBName;
        /// <summary>
        /// User name to connect
        /// </summary>
        public static string UserName;
        /// <summary>
        /// User password to connect
        /// </summary>
        public static string UserPassword;
        public static MySqlConnectionStringBuilder DBConnection;
        public static void MakeConnect()
        {
            DBConnection = DBConnector.Connect(
                ServerIP, DataBaseName,
                UserName, UserPassword);
        }
        public static MySqlConnectionStringBuilder MakeConnect(string UserName_, string UserPassword_)
        {
            UserName = UserName_;
            UserPassword = UserPassword_;
            MakeConnect();
            return DBConnection;
        }
        public static void SetServerIPViaURL(string URL)
        {
            ServerIP = System.Net.Dns.GetHostAddresses(URL)[0].MapToIPv4().Address.ToString();
        }
    }
}
