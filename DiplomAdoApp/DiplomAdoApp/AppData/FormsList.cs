﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiplomAdoApp.AppData
{
    static class FormsList
    {
        public static StudentDetails StudentDetailsForm;
        public static MainPresentation MainForm;
        public static Settings SettingsForm;
        public static Login LoginForm;
    }
}
