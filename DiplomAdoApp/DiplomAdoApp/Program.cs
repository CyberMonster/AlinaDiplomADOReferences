﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using DiplomAdoApp.AppData;

namespace DiplomAdoApp
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //FormsList.MainForm = new MainPresentation();
            FormsList.SettingsForm = new Settings();
            FormsList.LoginForm = new Login();
            FormsList.StudentDetailsForm = new StudentDetails();

            Application.Run(FormsList.LoginForm);
        }
    }
}
