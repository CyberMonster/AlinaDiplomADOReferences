﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;

using DiplomAdoApp.AppData;

namespace DiplomAdoApp.MySqlFunctions
{
    static class DBConnector
    {
        /// <summary>
        /// Выполняет соединение с базой данных
        /// </summary>
        /// <param name="ServerIP"> IP of remote server </param>
        /// <param name="DataBaseName"> Name of needed database </param>
        /// <param name="UserName"> User name </param>
        /// <param name="UserPassword"> User password </param>
        public static MySqlConnectionStringBuilder Connect(string ServerIP, string DataBaseName, string UserName, string UserPassword)
        {
            return new MySqlConnectionStringBuilder() { Server = ServerIP, Database = DataBaseName, UserID = UserName, Password = UserPassword };
        }

        private static System.Data.DataTable ResultTable;

        public static System.Data.DataTable GetDate(string Query)
        {
            ConnectionInfo.MakeConnect();
            ResultTable = new System.Data.DataTable();
            using (MySqlConnection Connect = new MySqlConnection())
            {
                //System.Windows.Forms.MessageBox.Show("SQL: " + Query);
                Connect.ConnectionString = ConnectionInfo.DBConnection.ConnectionString;
                MySqlCommand Command = new MySqlCommand(Query, Connect);
                try
                {
                    Connect.Open();
                    using (MySqlDataReader DataReader = Command.ExecuteReader())
                    {
                        if (DataReader.HasRows)
                        {
                            ResultTable.Load(DataReader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message + '\n' + "SQL: " + Query);
                }
                Connect.Close();
            }
            return ResultTable;
        }
    }
}